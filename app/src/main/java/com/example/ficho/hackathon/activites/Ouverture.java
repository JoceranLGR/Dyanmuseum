package com.example.ficho.hackathon.activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ficho.hackathon.R;

public class Ouverture extends AppCompatActivity {

    private EditText nom;
    private EditText prenom;
    private EditText age;
    private Button suivant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createprofile1);

        nom = findViewById(R.id.creaNom);
        prenom = findViewById(R.id.creaPrenom);
        age = findViewById(R.id.creaAge);

        suivant = findViewById(R.id.creaSuivant);

        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){



                Intent next = new Intent(Ouverture.this,CreaPref.class);
                startActivity(next);
            }
        });
    }


}