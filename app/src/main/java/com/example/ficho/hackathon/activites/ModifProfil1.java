package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class ModifProfil1 extends Activity {
    private EditText nom;
    private EditText prenom;
    private EditText age;
    private RadioGroup sexe;
    private Button suivant, retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifprofile1);

        nom = findViewById(R.id.modifNom);
        prenom = findViewById(R.id.modifPrenom);
        age = findViewById(R.id.modifAge);
        suivant = findViewById(R.id.modifSuivant);
        retour = findViewById(R.id.modifRetour);


        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent next = new Intent(ModifProfil1.this,ModifProfil2.class);
                startActivity(next);
            }
        });

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent next = new Intent(ModifProfil1.this,MainActivity.class);
                startActivity(next);
            }
        });
    }
}
