package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class Profil extends Activity {

    private Button buttonRetour, buttonModif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.affichageprofil);

        buttonModif = findViewById(R.id.affModifProfil);
        buttonRetour = findViewById(R.id.affRetour);


        buttonModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(Profil.this, ModifProfil1.class);
                startActivity(next);
            }
        });

        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(Profil.this, MainActivity.class);
                startActivity(next);
            }
        });


    }

}
