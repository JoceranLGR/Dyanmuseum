package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ScrollView;

import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class CreaPref extends Activity {

    private Button envoyer;
    private ScrollView listeTheme;
    private EditText duree;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createprofile2);

        envoyer = findViewById(R.id.buttonEnvoiCreation);
     //   listeTheme = findViewById(R.id.scrollViewModif);
        duree = findViewById(R.id.modifDureeVisite);

        envoyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreaPref.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }


}
