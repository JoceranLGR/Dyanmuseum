package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class InfosComp extends Activity {
    private Button retour;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infoscomp);

        retour = findViewById(R.id.infosCompRetour);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(InfosComp.this, Parcours.class);
                startActivity(retour);
            }
        });

    }
}
