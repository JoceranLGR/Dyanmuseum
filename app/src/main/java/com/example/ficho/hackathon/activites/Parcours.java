package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;


import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class Parcours extends Activity {

    private Button retour, tab1, tab2, tab3, tab4, tab5, tab6, tab7;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parcours);

        retour = (Button) findViewById(R.id.parcoursRetour);
        tab1 = findViewById(R.id.tab1);
        tab2 = findViewById(R.id.tab2);
        tab3 = findViewById(R.id.tab3);
        tab4 = findViewById(R.id.tab4);
        tab5 = findViewById(R.id.tab5);
        tab6 = findViewById(R.id.tab6);
        tab7 = findViewById(R.id.tab7);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, MainActivity.class);
                startActivity(retour);
            }
        });

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
        tab7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent retour = new Intent(Parcours.this, InfosComp.class);
                startActivity(retour);
            }
        });
    }
}
