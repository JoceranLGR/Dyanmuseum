package com.example.ficho.hackathon.activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ficho.hackathon.R;

public class MainActivity extends AppCompatActivity {

    private Button buttonProfil, buttonModif, buttonParcours;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        buttonProfil = findViewById(R.id.buttonProfil);
        buttonModif = findViewById(R.id.buttonModifProfil);
        buttonParcours = findViewById(R.id.buttonParcours);

        buttonProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextProfil = new Intent(MainActivity.this, Profil.class);
                startActivity(nextProfil);
            }
        });

        buttonModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextModif = new Intent(MainActivity.this, ModifProfil1.class);
                startActivity(nextModif);
            }
        });

        buttonParcours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextParcours = new Intent(MainActivity.this, Parcours.class);
                startActivity(nextParcours);
            }
        });


        }
}
