package com.example.ficho.hackathon.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ficho.hackathon.R;

/**
 * Created by ficho on 18/11/2017.
 */

public class ModifProfil2 extends Activity {
   // private ScrollView listeTheme;
    private EditText duree;
    private Button envoyer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifprofile2);

        envoyer = findViewById(R.id.buttonModifEnvoi);
       //listeTheme = findViewById(R.id.scrollViewModif);
        duree = findViewById(R.id.modifDureeVisite);


        envoyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ModifProfil2.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }


}
